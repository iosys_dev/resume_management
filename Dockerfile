FROM python:3.7

ENV SRC_DIR /var/www
ENV PYTHONPATH $SRC_DIR

WORKDIR $SRC_DIR

ARG DB_ENGINE
ARG DB_HOST
ARG DB_PORT
ARG DB_NAME
ARG DB_USER
ARG DB_PASS

COPY app $SRC_DIR
RUN pip install -r requirements.txt \
    && python manage.py makemigrations \
    && python manage.py migrate \
    && python manage.py loaddata initialization

EXPOSE 8000/tcp

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
