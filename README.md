# 経歴書管理システム #

https://io-sys.atlassian.net/wiki/spaces/public/pages/743276552

## セットアップ手順
１．Dockerをインストール

- [Docker for Windows](https://docs.docker.com/docker-for-windows/install/)
- [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
- MacまたはLinuxの場合は、[こちら](https://qiita.com/DQNEO/items/da5df074c48b012152ee)もやっておくと良い

２．docker-composeをインストール

- https://docs.docker.com/compose/install/

３．環境変数を設定

- `.env` ファイルを作成して、環境に合わせて変数を設定する
```
cp .env.skelton .env
```

- 設定内容

| 変数 | 説明 | 必須 | デフォルト値 |
|-----------------------|------------------------------------|----------|---------|
| AWS_ACCESS_KEY_ID | AWSアクセスキー | ○ |  |
| AWS_SECRET_ACCESS_KEY | AWSシークレットキー | ○ |  |
| AWS_DEFAULT_REGION | AWSデフォルトリージョン | | |
| S3_MEDIA_BUCKET | ファイルアップロード先S3バケット名（*1） | ○ |  |
| DB_ENGINE | DBエンジン（postgresql/mysql/sqlite3) | | sqlite3 |
| DB_HOST | DBサーバ | △（*2） |  |
| DB_PORT | DBポート | △（*2） |  |
| DB_NAME | データベース名 | △（*2） |  |
| DB_USER | DBユーザ | △（*2）|  |
| DB_PASS | DBパスワード | △（*2） |  |
| MAIL_HOST | SMTPサーバ |  |  |
| MAIL_PORT | SMTPポート |  | 587 |
| MAIL_USER | メールユーザ |  |  |
| MAIL_PASS | メールパスワード |  |  |
| MAIL_USE_TLS | TLSを使用（True/False） | | True |
| MAIL_FROM | 送信元アドレス |  |  |

- (*1) S3のバケットは自動作成するため、事前に作成しておく必要はない
- (*2) DB_ENGINEが `sqlite3` の場合は設定不要

４．ビルド
```
docker-compose build
```

## 起動
```
docker-compose up -d
```

- http://localhost:8000 にアクセスする

## 停止
```
docker-compose stop
```

