from django.urls import reverse
from django.views.generic import ListView, UpdateView, RedirectView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from resume.forms import FileUploadForm
from resume.models import File


class IndexView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return reverse("account_login")

        return reverse("file_list")


class FileListView(ListView):
    queryset = File.objects.all()
    context_object_name = "files"


class FileEditView(UpdateView):
    model = File
    form_class = FileUploadForm


class FileUploadView(APIView):
    def post(self, request):
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return Response(status=status.HTTP_201_CREATED)

        return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)


class FileDownloadView(APIView):
    def get(self, request, file_id):
        file = File.objects.get(id=file_id)
        file.dl_count += 1
        file.save()
        return Response(status=status.HTTP_200_OK)
