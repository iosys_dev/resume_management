from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse


class File(models.Model):
    name = models.CharField(max_length=1024)
    initials = models.CharField(null=True, max_length=8)
    age = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(99)])
    years_of_experience = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(99)])
    is_downloadable = models.BooleanField(default=True)
    dl_count = models.IntegerField(default=0)
    url = models.CharField(max_length=1024)
    memo = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("file_list")
