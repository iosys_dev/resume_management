from django.contrib import admin
from django.contrib.admin import ModelAdmin

from resume.models import File


@admin.register(File)
class FileAdmin(ModelAdmin):
    list_display = ('id', 'name', 'url', 'created_at')
