import logging
from io import BytesIO
from uuid import uuid4
from zipfile import ZipFile, ZIP_DEFLATED

from django import forms
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

from resume.models import File

logger = logging.getLogger(__name__)


class FileUploadForm(forms.ModelForm):
    initials = forms.CharField(label='イニシャル', required=False,
                               widget=forms.TextInput(attrs={'placeholder': ''}))
    age = forms.IntegerField(label='年齢', required=False)
    years_of_experience = forms.IntegerField(label='経験年数', required=False)
    memo = forms.CharField(label='メモ', required=False,
                           widget=forms.Textarea(attrs={'placeholder': '', 'rows': 5}))
    is_downloadable = forms.BooleanField(label='ダウンロード可', required=False)
    file = forms.FileField(label='経歴書（更新）', required=False)

    class Meta:
        model = File
        fields = (
            "initials",
            "age",
            "years_of_experience",
            "memo",
            "file",
            "is_downloadable"
        )

    def save(self, commit=True):
        upload_file = self.files.get("file")
        if upload_file:
            io = BytesIO()
            with ZipFile(file=io, mode="w", compression=ZIP_DEFLATED) as zf:
                zf.writestr(upload_file.name, upload_file.read())
            zip_name = f"{uuid4().hex}.zip"
            file_name = default_storage.save(zip_name, ContentFile(io.getvalue()))
            self.instance.name = upload_file.name
            self.instance.url = default_storage.url(file_name)
            logger.info(f"uploaded {upload_file.name} => {self.instance.url}")

        return super().save(commit)
